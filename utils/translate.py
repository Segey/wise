#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
#   @file		translate.py
#   @brief		The main file translates lines obtained with a file mask
#   @author		S.Panin
#   @Copyright	S.Panin
#   @version	v.1.0
#   @date	    Created on 02 September 2014 y., 16:18
#   @TODO
#   @usage
#        translate.py -i  d:\Projects\MagnetOffice\Tools\XlateFinder\bin\Release\MAGNET.lng -o d:\Projects\MagnetOffice\source\vc110\x64\Release\bin\CCResource_RUS.lng
#
import os
import fileinput
import sys
import argparse
import shutil


def commandLineArguments():
    parser = argparse.ArgumentParser(
        version="The Endlines v.1.0 by S.Panin<dix75@mail.ru>")
    parser.add_argument("-i", "--input",  help="A imput File.")
    parser.add_argument("-o", "--output", help="A output File.")
    args = parser.parse_args()
    return args


def execute(args):
    l = ""
    b=True
    for line in fileinput.input(args.input, inplace=1):
        if(b is True):
            l = line
            b = False
        else:
            line = "[1-" + l[1:]
            b = True
        sys.stdout.write(line)

    print "file: {0:<50} - DONE".format(args.input)
    shutil.copy(args.input, args.output)


def main():
    args = commandLineArguments()
    if(os.path.exists(args.input) is False):
        print "The input file '{0}' cannot exists.".format(args.input)
    if(os.path.exists(args.output) is False):
        print "The output file '{0}' cannot exists.".format(args.output)
    execute(args)

if __name__ == "__main__":
    sys.exit(main())
