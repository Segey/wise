#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
#   @file		endlines.py
#   @brief		The main file converts files obtained with a file mask
#   @author		S.Panin
#   @Copyright	S.Panin
#   @version	v.1.0
#   @date	    Created on 15 July 2014 y., 12:04
#   @TODO
#   @usage  endlins.py ./ -f *.h *.cpp
#           endlins.py ./ -e -f *.h *.cpp
#
import os, re, time, fileinput, sys, argparse
from xml.dom import minidom

"""
    \code
        args = commandLineArguments()
        args.files or args.files[0]
        args.path
    \endcode
"""
def commandLineArguments():
    parser = argparse.ArgumentParser(version="The Endlines v.1.0 by S.Panin<dix75@mail.ru>")
    parser.add_argument("path", help="A Files path.")
    parser.add_argument("-f", "--files", nargs="*", default="*.*", help="A list of files: *.cpp")
    parser.add_argument("-e", "--echo", action="store_true", default=False, help="show files that can be updated and exit")
    args = parser.parse_args()
    return args

def updateFiles(files):
    result = []
    for file in files:
        if 0 == file.find("*.") : file = file.replace("*.", "\.")
        else:
            file = file.replace(".", "\.")
            file = file.replace("*", ".*")
        result.append(file)
    return result

def getReforFiles(files):
    if files[0] == "*.*": return "^.*$"
    files =  updateFiles(files)
    if len(files) == 1 : return "^.*{0}$".format(files[0])
    return "^.*({0})$".format("|".join(files))

def updateFile(file):
    for line in fileinput.input(file, inplace=1):
        if(-1 != line.find("\r")):
            line = line.rstrip("\r\n") + "\n";
        sys.stdout.write(line)

def execute(args, regexp):
    for root, dirs, files in os.walk(args.path):
        for name in files:
            file = os.path.join(root, name)
            prog = re.compile(regexp)
            if(None == prog.match(file)): continue
            if (False == args.echo):
                updateFile(file)
                print "file: {0:<50} - DONE".format(name)
            else:
                print "file: {0:<50}".format(name)


def main():
    args = commandLineArguments()
    regexp =  getReforFiles(args.files)
    execute(args, regexp)

if __name__ == "__main__":
    sys.exit(main())
