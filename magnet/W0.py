#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
#   @file		W0_strings_not_used.py
#   @brief		The main file findes all unnecessary IDS_* strings
#      in a given directory
#   @author		S.Panin
#   @Copyright	topcon.com
#   @version	v.1.1
#   @create	    15 July 2014 y., 15:58
#   @update	    15 August 2014 y., 12:07
#   @TODO
#
import os
import re
import sys
import argparse
from rc import Rc
from time import gmtime, strftime, time

"""
    \code
        args = commandLineArguments()
        args.path
    \endcode
"""


def find(n):
    name = n.lower()
    result = False
    if(name.find(".cpp") != -1):
        result = True
    elif(name.find(".h") != -1):
        result = True
    if(name == "resource.h"):
        result = False
    if(name.find(".rc") != -1):
        result = False
    return result


def commandLineArguments():
    parser = argparse.ArgumentParser(
        version="The W0 v.1.0 by S.Panin<spanin@topcon.com>")
    parser.add_argument("path", help="A Files path.")
    parser.add_argument("-e", "--echo", action="store_true", default=False,
                        help="show files that can be updated and exit")
    args = parser.parse_args()
    return args


def updateStrings(strings):
    result = {}
    for i in strings:
        if strings[i] is not None:
            result[i] = strings[i]
    return result


def execute(data, strings):
    for root, dirs, files in os.walk(data.path_):
        for name in files:
            if(False == find(name)):
                continue
            file = os.path.join(root, name)
            for line in open(file):
                if(-1 == line.find("IDS_")):
                    continue
                for i in strings:
                    if strings[i] is None:
                        continue
                    if(None is not re.match("^.*\W{0}\W.*$".format(i), line)):
                        strings[i] = None
            strings = updateStrings(strings)
            print "file: {0:<50} - DONE".format(name)
    return updateStrings(strings)


def saveWordData(path, strings):
    if(0 == len(strings)):
        return
    path = "./done/{0}/".format(os.path.basename(os.path.abspath(path)))
    if(False == os.path.exists(path)):
        os.makedirs(path)

    t = strftime("%d_%m_%Y", gmtime())
    out = open("{0}W0_{1}_{2}.txt".format(path, t, time()), "w")
    for i in strings:
        print >>out, "{0: <50}{1: <90}{2}".format(i, strings[i]["file"],
                                                  strings[i]["line"])
    out.close()


def main():
    args = commandLineArguments()
    rc = Rc(args.path)
    strings = rc.getStrings()
    count = len(strings)
    strings = execute(rc, strings)
    saveWordData(args.path, strings)

    print ("The {0} of {1} strings have been found".format(len(strings), count))

if __name__ == "__main__":
    sys.exit(main())
