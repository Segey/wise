#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
#   @file		W2.py
#   @brief		The main file finds the equals parts of if statements
#   @author		S.Panin
#   @Copyright	topcon.com
#   @version	v.1.1
#   @create	    30 July 2014 y., 13:09
#   @update	    15 August 2014 y., 18:25
#   @TODO
#
import os
import re
import sys
import argparse
from rc import Rc
from time import gmtime, strftime, time
from walk import walkByLine
import unittest


def find(file, n):
    name = n.lower()
    if(re.match("^.*\.(h|hpp|c|cpp)$", name)):
        return True
    return False


def commandLineArguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("path", help="A Files path.")
    parser.add_argument("-e", "--echo", action="store_true", default=False,
                        help="show files that can be updated and exit")
    parser.add_argument("-t", "--test", action="store_true", default=False,
                        help="Test files")
    args = parser.parse_args()
    return args


def isAllBreackets(line):
    return line.count('(') == line.count(')')


def extract(line, file, index):
    if(-1 == line.find("if")):
        return None
    if(False == isAllBreackets(line)):
        return None
    m = re.match("^.*\s*if\s*\((.+)(?:==|\|\||&&)(.+)\).*$", line)
    if(m):
        _1 = m.group(1).strip()
        _2 = m.group(2).strip()
        if(_1 == _2):
            return (line.strip(), file, index)
    return None


def execute(path):
    result = []
    lines = walkByLine(path, f=find)
    for file, index, line in lines:
        data = extract(line, file, index)
        if(None is not data):
            result.append(data)
    return result


def saveWordData(data, strings):
    if(0 == len(strings)):
        return
    path = "./done/{0}/".format(os.path.basename(os.path.abspath(data.path_)))
    if(False == os.path.exists(path)):
        os.makedirs(path)

    t = strftime("%d_%m_%Y", gmtime())
    out = open("{0}W2_{1}_{2}.txt".format(path, t, time()), "w")
    for i in strings:
        print >>out, "{1: <80}{2}\n{0}".format(i[0], i[1], i[2])
    out.close()


def main():
    args = commandLineArguments()
    if(args.test is True):
        return 0

    rc = Rc(args.path)
    strings = execute(args.path)
    saveWordData(rc, strings)
    print ("The {0} strings have been found!".format(len(strings)))


class Test(unittest.TestCase):
    def test(self):
        self.assertEqual(1, 1)


if __name__ == "__main__":
    sys.exit(main())
