#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
#   @file		W8.py
#   @brief		Finds duplicate IDC in resource.h
#   @author		S.Panin
#   @Copyright	topcon.com
#   @version	v.1.0
#   @create	    05 August 2014 y., 13:32
#   @update	    05 August 2014 y., 13:32
#   @TODO
#
import os
import re
import sys
import argparse
from time import gmtime, strftime, time
from walk import walkByLine


def commandLineArguments():
    parser = argparse.ArgumentParser(
        version="The W8 v.1.0 by S.Panin<spanin@topcon.com>")
    parser.add_argument("path", help="A Files path.")
    parser.add_argument("-e", "--echo", action="store_true", default=False,
                        help="show files that can be updated and exit")
    args = parser.parse_args()
    return args


def findRC(file, n):
    name = n.lower()
    if(name == 'resource.h'):
        return True
    return False


def init(path):
    result = {}
    prog = re.compile(r"^.*\s(IDC_\w*)\s.*$")

    lines = walkByLine(path, f=findRC)
    for file, index, line in lines:
        if(-1 == line.find("IDC_")):
                continue
        m = prog.match(line)
        if(m):
            s = m.group(1)
            if(s in result):
                x = result[s]
                x.append({"file": file, "line": index})
                result[s] = x
            else:
                result[s] = [{"file": file, "line": index}]
    return result


def findDublicates(strings):
    for i in strings.keys():
        if(len(strings[i]) == 1):
            del strings[i]
    return strings


def saveWordData(path, strings):
    if(0 == len(strings)):
        return
    path = "./done/{0}/".format(os.path.basename(os.path.abspath(path)))
    if(False == os.path.exists(path)):
        os.makedirs(path)

    t = strftime("%d_%m_%Y", gmtime())
    out = open("{0}W8_{1}_{2}.txt".format(path, t, time()), "w")
    for i in strings:
        print >> out, "{0}".format(i)
        for j in strings[i]:
            print >> out, "{0: <4}{2: <6}{1}".format(' ', j['file'], j['line'])
    out.close()


def main():
    args = commandLineArguments()
    strings = init(args.path)
    strings = findDublicates(strings)
    saveWordData(args.path, strings)
    print ("The {0} strings have been found".format(len(strings)))

if __name__ == "__main__":
    sys.exit(main())
