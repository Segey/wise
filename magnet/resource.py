#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
#   @file		rc.py
#   @brief		The main file provides a Rc class that contains all operations with *.rc files
#   @author		S.Panin
#   @Copyright	topcon.com
#   @version	v.1.1
#   @create	    15 July 2014 y., 16:59
#   @update	    28 July 2014 y., 12:33
#   @TODO
#
import os
import re

"""
    #from rc import Rc

    getRcFiles  returns all rc files in ['file']

    getStrings  returns all IDS_ strings in rc files in {'strings': {"file": "file", "line": line] }
"""


class Rc:
    def getRcFiles(self):
        result = []
        for root, dirs, files in os.walk(self.path_):
            for name in files:
                file = os.path.join(root, name)
                if(-1 == file.find("resource.h") and -1 == file.find("Resource.h")):
                    continue
                result.append(file)
        return result

    def getStrings(self):
        result = {}
        rc_files = self.getRcFiles()
        for file in rc_files:
            with open(file) as f:
                cx = 0
                for line in f:
                    cx = cx + 1
                    if(-1 == line.find("IDS_")):
                        continue
                    prog = re.compile(r"^.*\s(IDS_\w*)\s.*$")
                    m = prog.match(line)
                    if(None != m):
                        result[m.group(1)] = {"file": file, "line": cx}
        return result

    def getDialogs(self):
        result = {}
        rc_files = self.getRcFiles()
        for file in rc_files:
            cx = 0
            for line in open(file):
                cx = cx + 1
                if(-1 == line.find("IDD_")):
                    continue
                prog = re.compile(r"^.*(IDD_[^ ]+) D.*$")
                m = prog.match(line)
                if(m is not None):
                    result[m.group(1)] = {"file": file, "line": cx}
        return result

    def folderName(self):
        m = re.match(r"^.*/([^/]+)/$", self.folder_)
        if(m):
            return m.group(1)
        return "xxx"

    def __init__(self, path):
        self.path_ = path
