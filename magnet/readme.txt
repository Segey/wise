W0  - Strings (IDS_*) that exist in *.rc files haven't represented in the project (*.cpp|*.h files)
W1  - Dialogs (IDD_*) that exist in *.rc files haven't represented in the project (*.cpp|*.h files)
W2  - Finds the equals parts in the if statements 
W3  - Finds the % symbols in _XLATE strings
W4  - Finds the {0} sequence in the _XLATE strings
W5  - Finds full placeholder sequence for XLATE strings
W6  - Finds duplicate IDS stirngs
W7  - Finds duplicate IDS stirngs in resource.h
W8  - Finds duplicate IDC in resource.h
W9  - Finds duplicate IDR in resource.h

