#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
#   @file		W10.py
#   @brief		Finds LinkControl functiion without the _T prefix
#   @author		S.Panin
#   @Copyright	topcon.com
#   @version	v.1.0
#   @create	    09 September 2014 y., 13:42
#   @update	    09 September 2014 y., 13:42
#   @TODO
#
import os
import re
import sys
import argparse
from time import gmtime, strftime, time
from walk import walkByLine


def commandLineArguments():
    parser = argparse.ArgumentParser(
        version="The W10 v.1.0 by S.Panin<spanin@topcon.com>")
    parser.add_argument("path", help="A Files path.")
    parser.add_argument("-e", "--echo", action="store_true", default=False,
                        help="show files that can be updated and exit")
    args = parser.parse_args()
    return args


def find(file, n):
    name = n.lower()
    if(name == "stdafx.cpp"):
        return False
    if(re.match(r"^.*\.(c|cpp)$", name)):
        return True
    return False


def init(path):
    result = {}

    lines = walkByLine(path, f=find)
    for file, index, line in lines:
        if(-1 == line.find("LinkControl(")):
                continue
        if(-1 != line.find("LinkControl()")):
                continue
        if(-1 != line.find("_T(\"")):
                continue
        if(-1 == line.find("\"")):
                continue
        if(file in result):
            x = result[file]
            x.append({"text": line, "line": index})
            result[file] = x
        else:
            result[file] = [{"text": line, "line": index}]
    return result


def saveWordData(path, strings):
    if(0 == len(strings)):
        return
    path = "./done/{0}/".format(os.path.basename(os.path.abspath(path)))
    if(False == os.path.exists(path)):
        os.makedirs(path)

    t = strftime("%d_%m_%Y", gmtime())
    out = open("{0}W10_{1}_{2}.txt".format(path, t, time()), "w")
    for file in strings:
        print >> out, "{0}".format(file)
        for j in strings[file]:
            print >> out, "{0: <4}{1: <6}{2}".format(' ', j['line'], j['text']),
    out.close()


def main():
    args = commandLineArguments()
    strings = init(args.path)
    saveWordData(args.path, strings)
    print ("The {0} strings have been found".format(len(strings)))

if __name__ == "__main__":
    sys.exit(main())
