#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
#   @file	walk.py
#   @brief	The Base file provides Base class that provides walk by project
#   @author	S.Panin
#   @Copyright	S.Panin
#   @version	v.1.2
#   @create	30 July  2014 y., 13:09
#   @update     24 March 2021 y., 14:59
#   @TODO
#
import os


def walkByLine(path, f):
    '''
        lines = walkByLine(path, f=find)
        for file, index, line in lines:
    '''
    for root, dirs, files in os.walk(path):
        for name in files:
            file = os.path.join(root, name)
            if(False == f(file, name)):
                continue
            index = 0
            for line in open(file):
                index += 1
                yield(file, index, line)
            print(f"file: {name:<50} - DONE")


def walkByFile(path, f):
    '''
        files = walkByFile(path, f=find)
        for file, name in files:
    '''
    for root, dirs, files in os.walk(path):
        for name in files:
            if(False == f(name)):
                continue
            file = os.path.join(root, name)
            yield(file, name)
            print(f"file: {name:<50} - DONE")
