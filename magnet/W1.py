#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
#   @file		W1_dialogs_not_used.py
#   @brief		The main file findes all unnecessary IDD_* dialogs
#               in a given directory
#   @author		S.Panin
#   @Copyright	topcon.com
#   @version	v.1.3
#   @create	    28 July 2014 y., 15:58
#   @update	    29 July 2014 y., 15:06
#   @TODO
#
import os
import re
import sys
import argparse
from rc import Rc
from time import gmtime, strftime, time

"""
    \code
        args = commandLineArguments()
        args.path
    \endcode
"""


def find(n):
    name = n.lower()
    result = False
    if(name.find(".cpp") != -1):
        result = True
    elif(name.find(".h") != -1):
        result = True
    if(name == "resource.h"):
        result = False
    if(name.find(".rc") != -1):
        result = False
    return result


def commandLineArguments():
    parser = argparse.ArgumentParser(
        version="The W1 v.1.0 by S.Panin<spanin@topcon.com>")
    parser.add_argument("path", help="A Files path.")
    parser.add_argument("-e", "--echo", action="store_true", default=False,
                        help="show files that can be updated and exit")
    args = parser.parse_args()
    return args


def updateDialogs(dialogs):
    result = {}
    for i in dialogs:
        if dialogs[i] is not None:
            result[i] = dialogs[i]
    return result


def execute(data, dialogs):
    for root, dirs, files in os.walk(data.path_):
        for name in files:
            if(False == find(name)):
                continue
            file = os.path.join(root, name)
            for line in open(file):
                if(-1 == line.find("IDD_")):
                    continue
                for i in dialogs:
                    if dialogs[i] is None:
                        continue
                    if(None is not re.match("^.*\W{0}\W.*$".format(i), line)):
                        dialogs[i] = None
            dialogs = updateDialogs(dialogs)
            print "file: {0:<50} - DONE".format(name)
    return updateDialogs(dialogs)


def saveWordData(path, dialogs):
    if(0 == len(dialogs)):
        return
    path = "./done/{0}/".format(os.path.basename(os.path.abspath(path)))
    if(False == os.path.exists(path)):
        os.makedirs(path)

    t = strftime("%d_%m_%Y", gmtime())
    out = open("{0}W1_{1}_{2}.txt".format(path, t, time()), "w")
    for i in dialogs:
        print >>out, "{0: <40}{1: <80}{2}".format(i, dialogs[i]["file"],
                                                  dialogs[i]["line"])
    out.close()


def main():
    args = commandLineArguments()
    rc = Rc(args.path)
    dialogs = rc.getDialogs()
    count = len(dialogs)
    dialogs = execute(rc, dialogs)
    saveWordData(args.path, dialogs)
    print ("The {0} of {1} dialogs have been found".format(len(dialogs), count))

if __name__ == "__main__":
    sys.exit(main())
