#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
#   @file		W5.py
#   @brief		Finds full placeholder sequence for XLATE strings
#   @author		S.Panin
#   @Copyright	topcon.com
#   @version	v.1.1
#   @create	    04 August 2014 y., 17:53
#   @update	    04 August 2014 y., 17:53
#   @TODO
#
import os
import re
import sys
import argparse
from time import gmtime, strftime, time
from walk import walkByLine


def find(file, n):
    name = n.lower()
    if(name == "resource.h"):
        return False
    if(re.match(r"^.*\.(h|hpp|c|cpp)$", name)):
        return True
    return False


def commandLineArguments():
    parser = argparse.ArgumentParser(
        version="The W5 v.1.0 by S.Panin<spanin@topcon.com>")
    parser.add_argument("path", help="A Files path.")
    parser.add_argument("-e", "--echo", action="store_true", default=False,
                        help="show files that can be updated and exit")
    args = parser.parse_args()
    return args


def execute(path):
    result = []
    lines = walkByLine(path, f=find)
    for file, index, line in lines:
        if(-1 == line.find("Xlatefmt")):
                continue
        if(-1 == line.find("{1}")):
                continue
        m = re.match(r"^.*Xlatefmt.*_XLATE\s*\(\"([^\"]+)\"(.*)$", line)
        if(m):
            for x in xrange(1, 10):
                if(m.group(1).find("{" + str(x) + "}") == -1):
                    break

            cx_y = m.group(2).count(',')
            if(x - 1 != cx_y):
                result.append((line.strip(), file, index, x - 1, cx_y))
    return result


def saveWordData(path, strings):
    if(0 == len(strings)):
        return
    path = "./done/{0}/".format(os.path.basename(os.path.abspath(path)))
    if(False == os.path.exists(path)):
        os.makedirs(path)

    t = strftime("%d_%m_%Y", gmtime())
    out = open("{0}W5_{1}_{2}.txt".format(path, t, time()), "w")
    for i in strings:
        print >> out, "{1: <80}{2}\n{0}\n{3} {{x}} sequence not equal {4},".\
            format(i[0], i[1], i[2], i[3], i[4])
    out.close()


def main():
    args = commandLineArguments()
    strings = execute(args.path)
    saveWordData(args.path, strings)
    print ("The {0} strings have been found".format(len(strings)))

if __name__ == "__main__":
    sys.exit(main())
