#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
#   @file		W2_test.py
#   @brief		The main file that tests the W2 project
#   @author		S.Panin
#   @Copyright	topcon.com
#   @version	v.1.0
#   @update	    15 August 2014 y., 18:27
#   @update	    15 August 2014 y., 18:27
#   @TODO
#
import W2
import unittest

file = "test.h"

class Test(unittest.TestCase):
    def testSimple(self):
        result = W2.extract("if(x==x)", "test.h", 1)
        self.assertEqual(("if(x==x)", "test.h", 1), result)

        result = W2.extract("if ( x ==x)", "test.h", 1)
        self.assertEqual(("if ( x ==x)", "test.h", 1), result)

    def testBrackets(self):
        data = "if (x == x)"
        result = W2.extract(data, file, 1)
        self.assertEqual((data, file, 1), result)

        data = "if ((x == x)"
        result = W2.extract(data, file, 1)
        self.assertEqual(None, result)

    #def testThreElements(self):
        #data = "if ( x == x && y == x)"
        #result = W2.extract(data, "test.h", 1)
        #print data.count('(')
        #self.assertEqual((data, "test.h", 1), result)

if __name__ == "__main__":
    unittest.main()
