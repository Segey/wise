#!/usr/bin/python
# -*-coding: UTF-8 -*-
#
#   @file		W3.py
#   @brief		Finds the % symbols in _XLATE strings
#   @author		S.Panin
#   @Copyright	topcon.com
#   @version	v.1.1
#   @create	    04 August 2014 y., 15:53
#   @update	    04 August 2014 y., 15:53
#   @TODO
#
import os
import re
import sys
import argparse
from time import gmtime, strftime, time
from walk import walkByLine


def find(file, n):
    name = n.lower()
    if(name == "resource.h"):
        return False
    if(re.match(r"^.*\.(h|hpp|c|cpp)$", name)):
        return True
    return False


def commandLineArguments():
    parser = argparse.ArgumentParser(
        version="The W3 v.1.0 by S.Panin<spanin@topcon.com>")
    parser.add_argument("path", help="A Files path.")
    parser.add_argument("-e", "--echo", action="store_true", default=False,
                        help="show files that can be updated and exit")
    args = parser.parse_args()
    return args


def execute(path):
    result = []
    lines = walkByLine(path, f=find)
    for file, index, line in lines:
        if(-1 == line.find("_XLATE")):
                continue
        if(-1 == line.find("%")):
                continue
        m = re.match(r"^.*Xlate.*_XLATE\s*\(\"([^\"]+).*$", line)
        if(m):
            m = re.match(r"^.*%\w.*$", m.group(1))
            if(m):
                result.append((line.strip(), file, index))
    return result


def saveWordData(path, strings):
    if(0 == len(strings)):
        return
    path = "./done/{0}/".format(os.path.basename(os.path.abspath(path)))
    if(False == os.path.exists(path)):
        os.makedirs(path)

    t = strftime("%d_%m_%Y", gmtime())
    out = open("{0}W3_{1}_{2}.txt".format(path, t, time()), "w")
    for i in strings:
        print >> out, "{1: <80}{2}\n{0}".format(i[0], i[1], i[2])
    out.close()


def main():
    args = commandLineArguments()
    strings = execute(args.path)
    saveWordData(args.path, strings)
    print ("The {0} strings have been found".format(len(strings)))

if __name__ == "__main__":
    sys.exit(main())
